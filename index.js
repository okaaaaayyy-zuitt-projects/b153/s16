
let number = 200;
console.log(`The number you provided is ${number}`)
for(let x = number; x >= 0; x--){
	if (x <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if(x % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}else if(x % 5 === 0){
		console.log(x);
	};
};

let string = "supercalifragilisticexpialidocious";
console.log(string);
let newString = ''

for(let i = 0; i < string.length; i++){
	if(
		string[i] === "a" ||
		string[i] === "e" ||
		string[i] === "i" ||
		string[i] === "o" ||
		string[i] === "u"
	){
		continue
	}else{
		newString += string[i];
	}	
}

console.log(newString);